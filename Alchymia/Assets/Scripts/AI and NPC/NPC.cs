﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour
{
    public GameObject Goal;
    public Dialogue dialogue;
    public int affectionLevel;
    protected NavMeshAgent agent;
    public Vector2[] positions;
    public Vector2Int[] times;
    public Animator npcAnimator;
    int num;
    // For dialogue field
    public int dialogue_field;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //agent.destination = positions[0];
        agent.updateUpAxis = false;
        agent.updateRotation = false;
        agent.stoppingDistance = 2f;
        num = 0;
        agent.isStopped = false;
        agent.SetDestination(positions[0]);
    }

    public void ChangeNPCOrientation(float isMoving) {
        int direction = 1;

        if (agent.velocity.y > 0) {
            direction = 2;
        }

        npcAnimator.SetInteger("Direction", direction);
        npcAnimator.SetFloat("IsMoving", isMoving);
    }

    // Update is called once per frame
    void Update()
    {
        if (TimeManager.IsPaused())
        {
            agent.isStopped = true;
            //Debug.Log("NPC stopped");
            return;
        }
        else if (DialogueManager.dialogueMode == true)
        {
            agent.isStopped = true;
        }
        else
        {
            agent.isStopped = false;
        }

        if (agent.remainingDistance <= 2.1f)
        {
            //agent.destination = positions[Random.Range(0, positions.Length)];
            agent.isStopped = true;
        }

        if (num < positions.Length && TimeCounter.TimeEqual(times[num].x, times[num].y))
        {
            //Debug.Log("On Time"+num);
            agent.isStopped = false;
            agent.destination = positions[num];
            num++;
        } else if (num == positions.Length)
        {
            num = 0;
        }

        float isMoving = agent.velocity.magnitude;
        ChangeNPCOrientation(isMoving);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        //Output the Collider's GameObject's name
        if (collision.collider == Goal)
        {
            agent.isStopped = true;
        }

    }

    public void startDialogue()
    {
        Debug.Log("in start dialogue of NPC");
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue, this);

    }
}
