﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EndNPC : MonoBehaviour
{
    public Dialogue dialogue;
    protected NavMeshAgent agent;
    public Vector2 positions;
    public Animator npcAnimator;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //agent.destination = positions[0];
        agent.updateUpAxis = false;
        agent.updateRotation = false;
        agent.stoppingDistance = 2f;
        agent.isStopped = false;
        agent.SetDestination(positions);
    }

    // Update is called once per frame
    void Update()
    {
       /* if (DialogueManager.dialogueMode == true)
        {
            Debug.Log("dialog mode is true");
            agent.isStopped = true;
        }
        else
        {
            agent.isStopped = false;
        }
        */

       /* if (num < positions.Length && TimeCounter.TimeEqual(times[num].x, times[num].y))
        {
            //Debug.Log("On Time"+num);
            agent.isStopped = false;
            agent.destination = positions[num];
            num++;

       }*/
        float isMoving = agent.velocity.magnitude;
        ChangeNPCOrientation(isMoving);
    }


    public void ChangeNPCOrientation(float isMoving)
    {
        int direction = 1;

        if (agent.velocity.y > 0)
        {
            direction = 2;
        }

        npcAnimator.SetInteger("Direction", direction);
        npcAnimator.SetFloat("IsMoving", isMoving);
    }

}
