﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EndMoveToClick : MonoBehaviour
{
    public static NavMeshAgent agent;
    private int isNextHit = 0;
    private DialogueTrigger dialTrig;
    public Sprite protagonistBack;
    public Sprite protagonistSide;
    public Sprite protagonistFront;
    public Animator protagonistAnimator;
    public float velocityThreshold = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //agent.velocity = new Vector3(4,4);
        agent.updateUpAxis = false;
        agent.updateRotation = false;
        agent.stoppingDistance = 0f;
        dialTrig = new DialogueTrigger();
    }

    // Update is called once per frame
    void Update()
    {
        float isMoving = agent.velocity.magnitude;
        float playerAngle = GetPlayerAngle(agent);
        ChangePlayerOrientation(playerAngle, isMoving);
        Click();
    }


    public float GetPlayerAngle(NavMeshAgent player)
    {
        Vector3 pos = Camera.main.WorldToScreenPoint(player.transform.position);
        pos = Input.mousePosition - pos;
        float angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
        if (angle < 0f)
        {
            angle += 360.0f;
        }
        return angle;
    }

    public void ChangePlayerOrientation(float playerAngle, float isMoving)
    {
        int direction = 1;
        //Turn Player Right
        if ((playerAngle >= 315f && playerAngle <= 360f) || (playerAngle <= 45f && playerAngle >= 1f))
        {
            direction = 3;
        }
        //Turn Player Up
        else if (playerAngle > 45f && playerAngle <= 135f)
        {
            direction = 2;
        }
        //Turn Player Left
        else if (playerAngle > 135f && playerAngle <= 225f)
        {
            direction = 4;
        }
        else
        {
            direction = 1;
        }

        protagonistAnimator.SetInteger("Direction", direction);
        protagonistAnimator.SetFloat("isMoving", isMoving);
    }

    public void Click()
    {
        // If a gameObject is clicked for the first time
        // record the hit and the gameObject and set the stopping distance to 2.5f
        Vector3 mousePosBag = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePosBag2D = new Vector2(mousePosBag.x, mousePosBag.y);

        if (Input.GetMouseButtonDown(0))
        {
            SoundManagerScript.PlaySound("dialogue");
                // Move to the gameObject
                Debug.Log("moving");
                Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 worldPoint2d = new Vector2(worldPoint.x, worldPoint.y);
                agent.destination = worldPoint2d;
                //Debug.Log("next hit(line 69" + isNextHit);
        }
    }
}
