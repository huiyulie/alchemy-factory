﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Affection {
    public Image level1;
    public Image level2;
}



public class PausedSystem : MonoBehaviour
{
    public static List<string> names = new List<string> {  "Eskhara visser", "Eupherein_Taliare", "Exciere Coquus", "Nervus Firmarius", "Teorian Doc" };

    const int MaxAffectionNum = 2;
    const int MaxPlayerNum = 5;
    public Affection[] affection;
    public GameObject[] players;
    public GameObject pauseUI;
    GameObject[] tagNPC;
    public Text time_text;

    void Start() {

        //Image[] img = players[0].GetComponentInChildren<Image[]>();
        pauseUI.gameObject.SetActive(false);
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void PausedOnClick() {
        if(TimeManager.menuMode == true)
        {
            SoundManagerScript.PlaySound("woodButton");
            pauseUI.gameObject.SetActive(true);
            time_text.text = TimeCounter.timeStr;

            tagNPC = GameObject.FindGameObjectsWithTag("NPC");
            foreach (GameObject npc in tagNPC)
            {
                NPC NPCobj;
                //Debug.Log("NPC's name is: " + npc.name);
                NPCobj = (NPC)npc.GetComponent<NPC>();
                for (int i = 0; i < names.Count; i++)
                {
                    if (names[i] == npc.name)
                    {
                        Debug.Log("names[i]" + names[i] + "npc name" + npc.name + "affection: " + NPCobj.affectionLevel);

                        AffectionSetActive(players[i], NPCobj.affectionLevel);
                    }
                }
            }
        }
        
    }

    public void AffectionSetActive(GameObject obj, int level)
    {
        Debug.Log("obj name is :" + obj.name);
        Debug.Log("obj affection level is:" + level);

        Sprite heart = Resources.Load<Sprite>("heart");
        Sprite heart_bg = Resources.Load<Sprite>("heart_bg");

        GameObject target_child = obj.transform.Find("Affection").gameObject;
        //Image[] image = target_child.GetComponents<Image>();
        //Debug.Log("image size" + image.Length);

        for (int i = 0; i < 6; i++)
        {
            
            if (i < level)
            {
                target_child.transform.GetChild(i).gameObject.GetComponent<Image>().sprite = heart;
            }
        }
        /*
        GameObject child = obj.transform.Find("Affection").gameObject;
        if (level == 0) {
            child.transform.GetChild(0).gameObject.SetActive(false);
            child.transform.GetChild(1).gameObject.SetActive(false);
        }
        else if(level == 1)
        {
            child.transform.GetChild(0).gameObject.SetActive(true);
            child.transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            child.transform.GetChild(0).gameObject.SetActive(true);
            child.transform.GetChild(1).gameObject.SetActive(true);
        }
        */

    }

    public void Play() {
        SoundManagerScript.PlaySound("retAlc");
        pauseUI.gameObject.SetActive(false);
    }

}
