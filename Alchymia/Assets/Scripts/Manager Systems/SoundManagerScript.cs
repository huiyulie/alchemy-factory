﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip dialogueSound, woodClickSound, moveSound, wrongSound, bagSound;

    public static AudioClip rightSound, returnAlcSound, resetSound, enterAteSound;
    //public static AudioClip otherSound;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        dialogueSound = Resources.Load<AudioClip>("whoosh"); //whoosh is present in resources folder
        woodClickSound = Resources.Load<AudioClip>("woodenbutton");
        moveSound = Resources.Load<AudioClip>("mechButton");
        wrongSound = Resources.Load<AudioClip>("WrongAnswer");
        rightSound = Resources.Load<AudioClip>("RightAnswer");
        bagSound = Resources.Load<AudioClip>("BagOpen");
        returnAlcSound = Resources.Load<AudioClip>("stashAlc");
        resetSound = Resources.Load<AudioClip>("reset");
        enterAteSound = Resources.Load<AudioClip>("achievement");


        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void PlaySound(string clip)
    {
        //Debug.Log("in play sound");
        switch (clip)
        {
            case "dialogue":
                audioSrc.PlayOneShot(dialogueSound);
                //called with SoundManagerScript.PlaySound("dialogue"); in the DialogueManager script
                break;
            case "woodButton":
                audioSrc.PlayOneShot(woodClickSound);
                break;
            case "moveClick":
                audioSrc.PlayOneShot(moveSound);
                break;
            case "bag":
                audioSrc.PlayOneShot(bagSound);
                break;
            case "wrong":
                audioSrc.PlayOneShot(wrongSound);
                break;
            case "right":
                audioSrc.PlayOneShot(rightSound);
                break;
            case "retAlc":
                audioSrc.PlayOneShot(returnAlcSound);
                break;
            case "reset":
                audioSrc.PlayOneShot(resetSound);
                break;
            case "atelier":
                audioSrc.PlayOneShot(enterAteSound);
                break;
        }
    }
}