﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class IntroDialogue : MonoBehaviour
{

    private List<string> sentences;
    public TextMeshProUGUI dialogueText;
    public Animator animator;
    public GameObject closeButton;
    private int currCount = 0;
    private bool sentenceDone = false;
    public static bool introSceneEnded = false;
    public GameObject blackBg;

    void Start()
    {
        sentences = new List<string>()
        {
            "A whole new world to explore! I'm so excited...",
            "So far I've only studied alchemy at school but now I get to help everyone with it...",
            "I'm going to start at Zetelan, get back to my mother's home town...",
            "She always described it as peaceful and secluded...",
            "I'm turning our ancestral home into my very own Atelier where I can experiment with my recipes...",
            "What a difference alchemy could make to the townsfolk! I really want to prove myself to them...",
            "I’ll start by talking to the townsfolk and understanding what they need... ",
            "I’ll collect ingredients on the way for my Alchemy products which will make the villagers happy!!!",
            "OH!!! I see the island now!!!"
        };
        //StartDialogue();

        Invoke("StartDialogue", 1);
    }
    public void StartDialogue()
    {
        animator.SetBool("isOpen", true);
        closeButton.SetActive(true);
        currCount = 0;

        //DisplayNextSentence();
        Invoke("DisplayNextSentence", 1);
    }

    public void DisplayNextSentence()
    {

        currCount += 1;
        sentenceDone = false;
        string sentence = sentences[currCount];
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
        //sentenceDone = true;
    }
    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
        sentenceDone = true;
        Debug.Log("s2: " + sentenceDone);
        if (currCount == sentences.Count - 1)
        {
            Invoke("EndDialogue", 3);
            //EndDialogue();
        }
        else
        {
            Invoke("DisplayNextSentence", 2);
            //DisplayNextSentence();
        }
    }

    public void EndDialogue()
    {

        //SoundManagerScript.PlaySound("dialogue");
        closeButton.SetActive(false);
        animator.SetBool("isOpen", false);

        Debug.Log("End of Conversation.");
        sentenceDone = false;
        Invoke("EndScene", 2);
    }
    public void EndScene()
    {
        SceneManager.LoadScene("Menu");
        //blackBg.SetActive(true);
        //introSceneEnded = true;
        //StartCoroutine(Camera.main.GetComponent<SceneFader>().FadeAndLoadScene(SceneFader.FadeDirection.In, "Menu"));
    }
}
