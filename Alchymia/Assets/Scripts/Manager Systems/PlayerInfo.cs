﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerInfo
{

    public List<string> NPCnames;
    public int[] affLevel;
    public List<string> alcItemsList;
    public List<string> ingItemsList;
    public int timeHour;
    public int timeMin;
    public int timeDay;
    public float[] playerPos;
    public List<float[]> NPCposList;    //npc's position according to alphabetical order.

    public int[] dialogue_field;

    // Get All information and is called in SaveData.SavePlayer() to save data.



    

   


    

}
