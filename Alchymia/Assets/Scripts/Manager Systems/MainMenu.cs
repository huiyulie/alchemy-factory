﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;



public class MainMenu : MonoBehaviour
{
    public GameObject blackbg;

    private void Start()
    {
        blackbg.SetActive(false);
    }
    public void PlayGame()
    {
        SoundManagerScript.PlaySound("woodButton");
        DeleteFile();
        SceneManager.LoadScene("MoveTest");
        //blackbg.SetActive(true);
        //StartCoroutine(Camera.main.GetComponent<SceneFader>().FadeAndLoadScene(SceneFader.FadeDirection.Out, "MoveTest"));

    }

    public void ContinuePlay() {
        blackbg.SetActive(true);
        SceneManager.LoadScene("MoveTest");
    }


    public void PlayIntro()
    {
        blackbg.SetActive(true);
        //StartCoroutine(Camera.main.GetComponent<SceneFader>().FadeAndLoadScene(SceneFader.FadeDirection.Out, "MoveTest"));
        SceneManager.LoadScene("Intro2D");

    }

    public void QuitGame()
    {
        
        blackbg.SetActive(true);
        Debug.Log("QUIT!");
        Application.Quit();
    }

    void DeleteFile()
    {
        string path = Application.persistentDataPath + "/player.fun";
        // check if file exists
        if (!File.Exists(path))
        {
            Debug.Log( "no " + path + " file exists"); //Debug.Log( "no " + fileName + " file exists" );
        }
        else
        {
            Debug.Log(" file exists, deleting..."); //Debug.Log( fileName + " file exists, deleting..." );

            File.Delete(path);

            RefreshEditorProjectWindow();
        }
    }


    void RefreshEditorProjectWindow()
    {
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

}
