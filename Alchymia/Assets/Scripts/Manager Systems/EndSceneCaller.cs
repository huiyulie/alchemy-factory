﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSceneCaller : MonoBehaviour
{
    //public List<int> affLevel;
    public int maxAff;
    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public int[] GetAffectionList()
    {
        //List<string> NPCnames = PausedSystem.names;
        GameObject[] tagNPC = GameObject.FindGameObjectsWithTag("NPC");
        int[] affectionLevel = new int[tagNPC.Length];
        //foreach (GameObject npc in tagNPC)
        for (int i = 0; i<tagNPC.Length; i++)
        {
            NPC NPCobj;
            //Debug.Log("NPC's name is: " + npc.name);
            Debug.Log("NPC's name is: " + tagNPC[i].name);
            NPCobj = (NPC)tagNPC[i].GetComponent<NPC>();
            affectionLevel[i] = NPCobj.affectionLevel;
        }
        return affectionLevel;
    }
    public void checkEndScene()
    {
        maxAff = 0;
        int[] affLevel = GetAffectionList();
        for (int i = 0; i<affLevel.Length; i++)
        {
            if(affLevel[i] == 6)
            {
                maxAff += 1;
            }
        }
        if(maxAff == 5)
        {
            SceneManager.LoadScene("MoveTest_EndScene");
        }
    }
}
