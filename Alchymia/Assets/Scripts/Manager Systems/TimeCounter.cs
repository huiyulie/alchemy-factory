﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeCounter : MonoBehaviour
{

    private static int startDay = 1, startHour = 8, startMinute = 0;
    private static int day, hour, minute;
    public Text uiText;
    public static string timeStr;
    const int MINUTEOFDAY = 1440;    //a day has 1440 minutes
    const int MINUTEOFHOUR = 60;     //an hour has 60 mintutes
    int count = 0;

    // Start is called before the first frame update



    void OnEnable()
    {
        TimeManager.OnTimeAdvance += Advance;
    }

    // Update is called once per frame
    void OnDisable()
    {
        TimeManager.OnTimeAdvance -= Advance;
    }

    public void Advance()
    {
        count += 1;
        //Debug.Log("count:"+count);
        CountToTime(count);
    }


    public void CountToTime(int count)
    {
        int dayCount = 0, hourCount = 0, minCount = 0;

        // add start time to count
        count += TimeToCount(startDay, startHour, startMinute);
        while (count >= MINUTEOFDAY)
        {
            count -= MINUTEOFDAY;
            dayCount += 1;
        }
        while (count >= MINUTEOFHOUR)
        {
            count -= MINUTEOFHOUR;
            hourCount += 1;
        }
        minCount += count;
        day = dayCount;
        hour = hourCount;
        minute = minCount;
        ShowTime();
    }

    public int TimeToCount(int day, int hour, int minute)
    {
        return day * MINUTEOFDAY + hour * MINUTEOFHOUR + minute;
    }

    public static int GetCurrentCount()
    {
        return hour * MINUTEOFHOUR + minute;
    }

    public static int GetCurrentCount(int cur_hour, int cur_min) {
        return cur_hour * MINUTEOFHOUR + cur_min;
    }

    public void ShowTime()
    {
        string hour_str = "", min_str = "";

        if (hour < 10)
        {
            hour_str = "0" + hour.ToString();
        }
        else
        {
            hour_str = hour.ToString();
        }
        if (minute < 10)
        {
            min_str = "0" + minute.ToString();
        }
        else
        {
            min_str = minute.ToString();
        }
        if (minute % 10 == 0)
        {
            timeStr = hour_str + ":" + min_str + " Day " + day.ToString();
            uiText.text = timeStr;
        }

    }


    public static bool TimeEqual(int dayCount, int hourCount, int minuteCount)
    {
        return (day == dayCount && hour == hourCount && minute == minuteCount);
    }

    public static bool TimeEqual(int hourCount, int minuteCount)
    {
        return (hour == hourCount && minute == minuteCount);
    }

    // get current day
    public static int GetDay()
    {
        return day;
    }

    // get current hour
    public static int GetHour()
    {
        return hour;
    }

    // get current minute
    public static int GetMinute()
    {
        return minute;
    }

    public static void SetTime(int dayCount, int hourCount, int minCount) {
        startDay = dayCount;
        startHour = hourCount;
        startMinute = minCount;
    }
}

