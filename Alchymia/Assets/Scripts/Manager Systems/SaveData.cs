﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine.SceneManagement;



public class SaveData:MonoBehaviour
{
    public void SavePlayer()
    {
        SoundManagerScript.PlaySound("right");
        BinaryFormatter formatter = new BinaryFormatter();
        //string path = Path.Combine(Application.persistentDataPath, player.fun);
        string path = Application.persistentDataPath + "/player.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerInfo info = SavePlayerInfo();

        formatter.Serialize(stream, info);
        stream.Close();
    }

    public static PlayerInfo LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.fun";
        Debug.Log("The save file path is:" + path);
        if (File.Exists(path))
        {

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            PlayerInfo info = formatter.Deserialize(stream) as PlayerInfo;
            stream.Close();

            return info;
        }
        else
        {
            Debug.Log("Save file not found in " + path);
            return null;
        }
    }



    public PlayerInfo SavePlayerInfo()
    {

        Debug.Log("Before Saving the game.");
        PlayerInfo info = new PlayerInfo();
        //NPCnames = PausedSystem.names;
        info.NPCnames = PausedSystem.names;
        Debug.Log("Eupherein name is:" + info.NPCnames[0]);
        // Get Time info
        info.timeDay = TimeCounter.GetDay();
        info.timeHour = TimeCounter.GetHour();
        info.timeMin = TimeCounter.GetMinute();
        Debug.Log("Current time is:" + info.timeDay + ":" + info.timeHour + ":" + info.timeMin);


        // Get Affection level list
        info.affLevel = GetAffectionList();
        Debug.Log("Eupherein affection level is:" + info.affLevel[0]);

        // Get Protagolist's position
        GameObject Player = GameObject.FindGameObjectWithTag("Protagonist");
        info.playerPos = new float[] { Player.transform.position.x, Player.transform.position.y };
        Debug.Log("Save player position:" + "(" + Player.transform.position.x + "," + Player.transform.position.y + ")");

        // Get Protagonist's position and npc's position
        info.NPCposList = GetPosition();
        for (int i = 0; i < 5; i++) {
            Debug.Log(PausedSystem.names[i] + " position is:"+"(" + info.NPCposList[i][0] + "," + info.NPCposList[i][1] + ")");

        }



        //Get alchemy items List
        info.alcItemsList = GetAlchemyList();
        Debug.Log("Alchemy List length is:" + info.alcItemsList.Count);
        //Debug.Log("First Alchemy is:" + info.alcItemsList[0]);

        //Get ingredients items List
        info.ingItemsList = GetIngredientsList();
        Debug.Log("Ingredients List length is:" + info.ingItemsList.Count);
        //Debug.Log("First ingredient is:" + info.ingItemsList[0]);

        // For NPC dialogue field
        info.dialogue_field = GetDialogueList();

        return info;
    }

    
    // Load all information change all current states.
    private void Awake()
    {
        //Debug.Log("Scene Awake!!!");
        PlayerInfo playerInfo = LoadPlayer();
        if (playerInfo == null)
        {
            Debug.Log("No player record found");
            return;
        }
        List<string> NPCnames = playerInfo.NPCnames;

        //Set time
        //Debug.Log("Saved time is:(" + playerInfo.timeDay + "," + playerInfo.timeHour + "," + playerInfo.timeMin + ")");
        TimeCounter.SetTime(playerInfo.timeDay, playerInfo.timeHour, playerInfo.timeMin);

        //Set Protagolist's position
        GameObject Player = GameObject.FindGameObjectWithTag("Protagonist");
        //Debug.Log("Before Loading, player position is:" + "(" + Player.transform.position.x + "," + Player.transform.position.y + ")");
        //Debug.Log("Loading Player position:" + "(" + playerInfo.playerPos[0] + "," + playerInfo.playerPos[1] + ")");
        Player.transform.position = new Vector3(playerInfo.playerPos[0], playerInfo.playerPos[1], 0);
        //Debug.Log("After Loading, player position set to:" + "(" + Player.transform.position.x + "," + Player.transform.position.y + ")");


        GameObject[] tagNPC = GameObject.FindGameObjectsWithTag("NPC");
        Debug.Log("Loading NPC position:");
        for (int i = 0; i < tagNPC.Length; i++)
        {
            //Set NPC's Affection level and position
            NPC NPCobj = (NPC)tagNPC[i].GetComponent<NPC>();
            for (int j = 0; j < NPCnames.Count; j++)
            {
                if (NPCnames[j] == tagNPC[i].name)
                {
                    NPCobj.affectionLevel = playerInfo.affLevel[j];
                    NPCobj.dialogue_field = playerInfo.dialogue_field[j];
                    tagNPC[i].transform.position = new Vector3(playerInfo.NPCposList[j][0], playerInfo.NPCposList[j][1], 0);
                    Debug.Log(tagNPC[i].name + " position is:" + "(" + tagNPC[i].transform.position.x + "," + tagNPC[i].transform.position[1] + ")");

                }
            }

        }
        TimeManager.menuMode = false;

     }
     
    public void setInventory()
    {
        PlayerInfo playerInfo = LoadPlayer();
        if (playerInfo == null)
        {
            Debug.Log("No player record found");
            return;
        }

        GameObject ingObject = GameObject.Find("IngredientSpawning");
        //Debug.Log("Find IngredientSpawning object name is:" + ingObject.name);
        for (int i = 0; i < playerInfo.ingItemsList.Count; i++)
        {

            Sprite obj_sprite;
            foreach (Transform child in ingObject.transform)
            {
                if (child.name == playerInfo.ingItemsList[i])
                {
                    GameObject gObj = new GameObject();
                    obj_sprite = child.GetComponent<SpriteRenderer>().sprite;
                    //Debug.Log("INV SPRITE: " + obj_sprite.name);
                    gObj.AddComponent<SpriteRenderer>().sprite = obj_sprite;
                    //Debug.Log("INV SPRITE: " + gObj.GetComponent<SpriteRenderer>().sprite.name);
                    Inventory.AddIngredientItem(gObj, Item.Type.Ingredient, obj_sprite);
                }

            }

        }
       
        //Debug.Log("Before Loading, saved alchemy item is:" + playerInfo.alcItemsList[0]);
        //GameObject alcObject = GameObject.Find("AlchemyItems");
        //Debug.Log("Find Alchemy object name is:" + alcObject.name);

        /*

        List<string> alchemyItems = new List<string>;
        alchemyItems.Add("Medicine");
        alchemyItems.Add("Preservative");
        alchemyItems.Add("BaitSolution");
        alchemyItems.Add("Fuel");
        alchemyItems.Add("FireExtinguisher");
        alchemyItems.Add("Fertilizer");
        alchemyItems.Add("CleaningSolution");
        alchemyItems.Add("EnergyElixir");
        alchemyItems.Add("Ointment");
        alchemyItems.Add("Pesticide");
        alchemyItems.Add("LovePotion");
        alchemyItems.Add("SpeedPotion");*/

        List<Sprite> alchemyItems = new List<Sprite>();
        alchemyItems.Add(Resources.Load<Sprite>("Medicine"));
        alchemyItems.Add(Resources.Load<Sprite>("Preservative"));
        alchemyItems.Add(Resources.Load<Sprite>("BaitSolution"));
        alchemyItems.Add(Resources.Load<Sprite>("Fuel"));
        alchemyItems.Add(Resources.Load<Sprite>("FireExtinguisher"));
        alchemyItems.Add(Resources.Load<Sprite>("Fertilizer"));
        alchemyItems.Add(Resources.Load<Sprite>("CleaningSolution"));
        alchemyItems.Add(Resources.Load<Sprite>("EnergyElixir"));
        alchemyItems.Add(Resources.Load<Sprite>("Ointment"));
        alchemyItems.Add(Resources.Load<Sprite>("Pesticide"));
        alchemyItems.Add(Resources.Load<Sprite>("LovePotion"));
        alchemyItems.Add(Resources.Load<Sprite>("SpeedPotion"));


        for (int i = 0; i < playerInfo.alcItemsList.Count; i++)
        {
            Sprite obj_sprite;
            //Debug.Log("first alchemy item:" + playerInfo.alcItemsList[0]);
            foreach (Sprite alch_item in alchemyItems)
            {
                
                if (alch_item.name == playerInfo.alcItemsList[i])
                {
                    GameObject gObj = new GameObject();
                    //Debug.Log("Found saved alchemy Item");
                    obj_sprite = alch_item;
                    //Debug.Log("obj_sprite name is:" + obj_sprite.name);
                    gObj.AddComponent<SpriteRenderer>().sprite = obj_sprite;
                    Inventory.AddAlchemyItem(gObj, Item.Type.Alchemy, obj_sprite);
                }
            }


        }
    }

    public int[] GetAffectionList()
    {
        List<string> NPCnames = PausedSystem.names;
        GameObject[] tagNPC = GameObject.FindGameObjectsWithTag("NPC");
        int[] affectionLevel = new int[tagNPC.Length];
        foreach (GameObject npc in tagNPC)
        {
            NPC NPCobj;
            Debug.Log("NPC's name is: " + npc.name);
            NPCobj = (NPC)npc.GetComponent<NPC>();
            for (int i = 0; i < NPCnames.Count; i++)
            {
                if (NPCnames[i] == npc.name)
                {
                    affectionLevel[i] = NPCobj.affectionLevel;
                }
            }
        }
        return affectionLevel;
    }

    public int[] GetDialogueList() {
        List<string> NPCnames = PausedSystem.names;
        GameObject[] tagNPC= GameObject.FindGameObjectsWithTag("NPC");
        int[] dialogueList = new int[tagNPC.Length];
        foreach (GameObject npc in tagNPC) {
            NPC NPCobj;
            Debug.Log("NPC's name is: " + npc.name);
            NPCobj = (NPC)npc.GetComponent<NPC>();
            for (int i = 0; i < NPCnames.Count; i++)
            {
                if (NPCnames[i] == npc.name)
                {
                    dialogueList[i] = NPCobj.dialogue_field;
                }
            }
        }
        return dialogueList;
    }


    public List<float[]> GetPosition()
    {
        List<string> NPCnames = PausedSystem.names;


        GameObject[] tagNPC = GameObject.FindGameObjectsWithTag("NPC");
        List<float[]> posList = new List<float[]>();
        // Get NPCs' position
        foreach (GameObject npc in tagNPC)
        {
            for (int i = 0; i < NPCnames.Count; i++)
            {
                if (NPCnames[i] == npc.name)
                {
                    Vector2 npcPos = new Vector2(npc.transform.position.x, npc.transform.position.y);
                    float[] npcPos_f = new float[] {npcPos.x,npcPos.y };
                    posList.Add(npcPos_f);
                }
            }
        }
        return posList;

    }

    public void QuitToMenu() {
        SoundManagerScript.PlaySound("retAlc");
        SceneManager.LoadScene("Menu");
    }

    public void GetAlchemy() {

    }

    public List<string> GetIngredientsList() {
        List<string> ingredientsList = new List<string>();
        for (int i = 0; i < Inventory.ingredientSlots.Count; i++)
        {
            if (Inventory.ingredientSlots[i].GetComponent<Slot>().empty == false)
            {
                string ingredientsName = Inventory.ingredientSlots[i].GetComponent<Slot>().icon.name;
                //Debug.Log(newItem.iconSprite.name);
                ingredientsList.Add(ingredientsName);
            }

        }
        return ingredientsList;
    }

    public List<string> GetAlchemyList()
    {
        List<string> alchemyList = new List<string>();
        for (int i = 0; i < Inventory.alchemySlots.Count; i++)
        {
            if (Inventory.alchemySlots[i].GetComponent<Slot>().empty == false)
            {
                string alchemyName = Inventory.alchemySlots[i].GetComponent<Slot>().icon.name;
                //Debug.Log(newItem.iconSpriame);
                alchemyList.Add(alchemyName);
            }

        }
        return alchemyList;
    }

    
}