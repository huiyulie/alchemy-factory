﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;

    public AudioMixer soundAudioMixer;
    public Slider musicSlider;
    public Slider soundSlider;

    public static AudioClip trialSound;
    static AudioSource soundAudioSrc;

    void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0.75f);
        soundSlider.value = PlayerPrefs.GetFloat("SoundVolume", 0.75f);
        trialSound = Resources.Load<AudioClip>("stashAlc");
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("volume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }


    public void SetSoundVolume(float volume)
    {
        soundAudioSrc = GetComponent<AudioSource>();
        soundAudioMixer.SetFloat("soundVolume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("SoundVolume", volume);
        soundAudioSrc.PlayOneShot(trialSound);
    }

}
