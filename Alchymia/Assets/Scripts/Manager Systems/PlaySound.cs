﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayWoodOnClick()
    {
        SoundManagerScript.PlaySound("woodButton");
    }

    public void PlayRightOnClick()
    {
        SoundManagerScript.PlaySound("right");
    }

    public void PlaySoundOnClick(string s)
    {
        SoundManagerScript.PlaySound(s);
    }
}
