﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;


public class LightManager : MonoBehaviour
{
    public UnityEngine.Experimental.Rendering.LWRP.Light2D globalLight;
    public UnityEngine.Experimental.Rendering.LWRP.Light2D towerLight;
    public UnityEngine.Experimental.Rendering.LWRP.Light2D[] pointLight = new UnityEngine.Experimental.Rendering.LWRP.Light2D[5];
    private TimeCounter time = new TimeCounter();
    Color dayColor = new Color(1f, 1f, 1f);
    Color nightColor = new Color(0, 0.471f, 1.0f);
    Color sunsetColor = new Color(0.96f, 1f, 0.3f);
    float dayIntensity = 1.0f;
    float nightIntensity = 0.8f;

    int brighterStart, brighterEnd, sunsetStart, sunsetEnd, darkerEnd;


    // Start is called before the first frame update
    void Start()
    {
        brighterStart = time.TimeToCount(0, 8, 01);
        brighterEnd = time.TimeToCount(0, 10, 0);
        sunsetStart = time.TimeToCount(0, 16, 0);
        sunsetEnd = time.TimeToCount(0, 18, 0);
        darkerEnd = time.TimeToCount(0, 20, 0);
    }

    // Update is called once per frame
    void Update()
    {
        int currentTime = TimeCounter.GetCurrentCount();

        if (currentTime < brighterStart || currentTime > darkerEnd)
        {
            globalLight.color = nightColor;
            globalLight.intensity = nightIntensity;
        }

        if (currentTime > brighterEnd && currentTime < sunsetStart)
        {
            globalLight.color = dayColor;
            globalLight.intensity = dayIntensity;
        }

        // Turn off the light at 10:00 - 20:00
        if (currentTime > brighterEnd && currentTime < darkerEnd)
        {
            //Debug.Log("Day!");
            //globalLight.intensity = dayIntensity;
            //globalLight.color = dayColor;
            //Debug.Log(globalLight.color);
            towerLight.intensity = 0;
            foreach (var eachLight in pointLight)
            {
                eachLight.intensity = 0;
            }
        }
        else {
            //globalLight.intensity = nightIntensity;
            //globalLight.color = nightColor;
            towerLight.intensity = 1;
            foreach (var eachLight in pointLight)
            {
                eachLight.intensity = 1;
            }
        }

        
        //Debug.Log(darkerStart);
        //Debug.Log(darkerEnd);
        //Debug.Log(currentTime);
        float ratio = 0;
        if (currentTime > brighterStart && currentTime < brighterEnd)
        {
            ratio = LinearRatio(currentTime, brighterStart, brighterEnd);
            globalLight.color = Color.Lerp(nightColor, dayColor, ratio);
            globalLight.intensity = dayIntensity * ratio + nightIntensity * (1 - ratio);
            //Debug.Log(globalLight.color);
        }
        else if (currentTime > sunsetStart && currentTime < sunsetEnd)
        {
            ratio = LinearRatio(currentTime, sunsetStart, sunsetEnd);
            globalLight.color = Color.Lerp(dayColor, sunsetColor, ratio);
            // light intensity doesn't change
            //Debug.Log(ratio);
        }
        else if (currentTime > sunsetEnd && currentTime < darkerEnd) {
            ratio = LinearRatio(currentTime, sunsetEnd, darkerEnd);
            globalLight.color = Color.Lerp(sunsetColor, nightColor, ratio);
        }
    }

    public static float LinearRatio(float t, float t_start, float t_end)
    {
        return (t - t_start) / (t_end - t_start);

    }

}
