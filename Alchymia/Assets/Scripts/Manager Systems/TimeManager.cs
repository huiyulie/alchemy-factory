﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public float turnDuration = 1f;
    public static bool paused = false;
    public static bool menuMode = false;
    public float fastForwardMultiplier = 10f;
    public delegate void OnTimeAdvanceHandler();
    public static event OnTimeAdvanceHandler OnTimeAdvance;

    float advanceTimer;

    // Start is called before the first frame update
    void Start()
    {
        advanceTimer = turnDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if (!paused)
        {
            //advanceTimer -= Time.deltaTime * fastForwardMultiplier;
            advanceTimer -= Time.deltaTime*fastForwardMultiplier;
            if (advanceTimer <= 0)
            {
                advanceTimer += turnDuration;
                OnTimeAdvance?.Invoke();
            }
        }
        if(DialogueManager.dialogueMode == true || menuMode == true)
        {
            paused = true;
        }
        else
        {
            paused = false;
        }
        
    }

    public void Pause()
    {
        if(DialogueManager.dialogueMode != true)
        {
            menuMode = true;
            paused = true;
            IngredientSpawning.spawn = false;
        }
        
    }

    public void Play()
    {
        menuMode = false;
        paused = false;
        IngredientSpawning.spawn = true;
    }

    public static bool IsPaused()
    {
        return paused;
    }
}
