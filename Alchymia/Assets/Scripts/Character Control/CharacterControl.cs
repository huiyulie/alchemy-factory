﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    // Keep target of where the player is heading
    Vector2 targetPos;

    [SerializeField]
    float speed = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        targetPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        bool move = true;
        if (Input.GetMouseButtonDown(0)) {
            Debug.Log("1");
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            Debug.Log("2");
            if (hit.collider != null)
            {
                Debug.Log("3");
                if (hit.collider.gameObject.CompareTag("Bag"))
                {
                    Debug.Log("Bag");
                    move = false;
                    GameObject[] insideBag = GameObject.FindGameObjectsWithTag("Inside Bag");
                    foreach (GameObject gameObject in insideBag)
                    {
                        if (gameObject.GetComponent<Renderer>().enabled)
                        {
                            gameObject.GetComponent<Renderer>().enabled = false;
                        }
                        else
                        {
                            gameObject.GetComponent<Renderer>().enabled = true;
                        }
                    }
                }
            } else
            {
                targetPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                move = true;
            }
        }
        
        // Move player towards target position
        if (move && (Vector2)transform.position != targetPos) {
            transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        }
    }
}
