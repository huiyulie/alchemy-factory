﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using InputExtender;

public class MoveToClick : MonoBehaviour
{
    public GameObject AlchemyMenu;
    public static NavMeshAgent agent;
    private float distanceThreshold = 5f;
    private int isNextHit = 0;
    private GameObject clickedObj;
    private DialogueTrigger dialTrig;
    private NPC NPCobj;
    private bool pausedMode = false;
    private Vector2 pausedDestination;
    public GameObject[] ingredients;
    public Sprite protagonistBack;
    public Sprite protagonistSide;
    public Sprite protagonistFront;
    public Animator protagonistAnimator;
    public float velocityThreshold = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        //transform.position = new Vector3(14, 5);
        agent = GetComponent<NavMeshAgent>();
        //agent.velocity = new Vector3(4,4);
        agent.updateUpAxis = false;
        agent.updateRotation = false;
        agent.stoppingDistance = 0f;
        dialTrig = new DialogueTrigger();

    }
    // Update is called once per frame
    void Update()
    {
        if (TimeManager.IsPaused() || DialogueManager.dialogueMode == true)
        {
            agent.isStopped = true;
            pausedMode = true;
            agent.destination = pausedDestination;
            return;
        }
        if (pausedMode && !TimeManager.IsPaused()) {
            pausedMode = false;
            agent.isStopped = false;
            return;
        }

        float isMoving = agent.velocity.magnitude;
        //Debug.Log("Player Velocity " + isMoving);
        float playerAngle = GetPlayerAngle(agent);
        ChangePlayerOrientation(playerAngle, isMoving);
        Click();
    }

    public float GetPlayerAngle(NavMeshAgent player) {
        Vector3 pos = Camera.main.WorldToScreenPoint(player.transform.position);
        pos = Input.mousePosition - pos;
        float angle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
        if (angle < 0f) {
            angle += 360.0f;
        }
        return angle;
    }

    public void ChangePlayerOrientation(float playerAngle, float isMoving){
        int direction = 1;
        //Turn Player Right
        if ((playerAngle >= 315f && playerAngle <= 360f) || (playerAngle <= 45f && playerAngle >= 1f))
        {
            direction = 3;
        }
        //Turn Player Up
        else if (playerAngle > 45f && playerAngle <= 135f)
        {
            direction = 2;
        }
        //Turn Player Left
        else if (playerAngle > 135f && playerAngle <= 225f)
        {
            direction = 4;
        }
        else {
            direction = 1;
        }

        protagonistAnimator.SetInteger("Direction", direction);
        protagonistAnimator.SetFloat("isMoving", isMoving);
    }

    public void Click() {
        // If a gameObject is clicked for the first time
        // record the hit and the gameObject and set the stopping distance to 2.5f
        Vector3 mousePosBag = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePosBag2D = new Vector2(mousePosBag.x, mousePosBag.y);
        RaycastHit2D hitBag = Physics2D.Raycast(mousePosBag2D, Vector2.zero);

        if (Input.GetMouseButtonDown(0))
        {
            SoundManagerScript.PlaySound("dialogue");
            if (hitBag.collider != null)
            {
                agent.stoppingDistance = 2.5f;
                isNextHit++;
                clickedObj = hitBag.collider.gameObject;

                if (clickedObj.CompareTag("NPC") && isNextHit == 1)
                {
                    DialogueManager.dialogueMode = true;
                }
                else
                {
                    DialogueManager.dialogueMode = false;
                }
                if ((clickedObj.CompareTag("Fruit") || clickedObj.CompareTag("Jewel")) && isNextHit == 1) //checking ingredients to pick up
                {
                    DialogueManager.dialogueMode = true;
                }
                else
                {
                    DialogueManager.dialogueMode = false;
                }

                // If a new gameObject is clicked record the new gameObject
                if (clickedObj != null && clickedObj != hitBag.collider.gameObject)
                {
                    clickedObj = hitBag.collider.gameObject;
                    DialogueManager.dialogueMode = false;
                }
                else
                {
                    DialogueManager.dialogueMode = false;
                }
                // Move to the gameObject
                Debug.Log("moving");
                Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 worldPoint2d = new Vector2(worldPoint.x, worldPoint.y);
                pausedDestination = agent.destination;
                agent.destination = worldPoint2d;
                //Debug.Log("next hit(line 69" + isNextHit);

                if (isNextHit == 2 && hitBag.collider.gameObject == clickedObj)
                {
                    //Debug.Log("nexthit = " + isNextHit);
                    if (clickedObj.CompareTag("NPC"))
                    {
                        DialogueManager.dialogueStarted = true;
                        NPCobj = (NPC)clickedObj.GetComponent<NPC>();
                        NPCobj.startDialogue();
                    }
                    else if (clickedObj.CompareTag("Atelier"))
                    {

                        SoundManagerScript.PlaySound("atelier");
                        //SceneManager.LoadScene("alc2");
                        AlchemyMenu.SetActive(true);
                        AlchemyMenu.transform.GetChild(1).GetComponent<InventoryControl>().GenInventory();
                        AlchemyMenu.transform.GetChild(2).GetComponent<MixerControl>().ResetMixer();
                        DialogueManager.dialogueMode = true;
                        agent.isStopped = true;
                    }
                    else
                    {
                       DialogueManager.dialogueMode = false;
                    }
                    isNextHit = 0;
                }
                if (hitBag.collider.gameObject.CompareTag("Bag"))
                {
                    Debug.Log("Bagg");

                    SoundManagerScript.PlaySound("bag");
                    //agent.isStopped = true;
                    DialogueManager.dialogueMode = true;
                    Inventory.inventoryEnabled = true;
                    //Debug.Log(Inventory.inventoryEnabled);
                }
            }
            // If a non gameObject was clicked then set the next hit to false
            // and set the gameObject to null
            if (hitBag.collider == null && DialogueManager.dialogueStarted == false && Inventory.inventoryEnabled == false && !pausedMode)
            {
                //Debug.Log("Moving 103");
                agent.stoppingDistance = 0f;
                Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 worldPoint2d = new Vector2(worldPoint.x, worldPoint.y);
                pausedDestination = agent.destination;
                agent.destination = worldPoint2d;
                isNextHit = 0;
                clickedObj = null;
                DialogueManager.dialogueMode = false;
            }


        }
    }
    public void closeAtelier()
    {

        SoundManagerScript.PlaySound("atelier");
        AlchemyMenu.SetActive(false);
        DialogueManager.dialogueMode = false;
        agent.isStopped = false;
    }
}