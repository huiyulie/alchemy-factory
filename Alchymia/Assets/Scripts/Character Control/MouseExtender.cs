﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InputExtender
{
    public static class MouseExtender
    {
        private static float timer;

        private static float lastTimer;
        private static float currentTimer;
        private static int clicks = 0;
        private static float threshold = 0.25f;

        public static bool IsDoubleClick(int _mouseButton)
        {
            if (Input.GetMouseButtonDown(_mouseButton))
            {
                clicks += 1;

                if (clicks == 1)
                {
                    lastTimer = Time.unscaledTime;
                }

                if (clicks >= 2)
                {
                    currentTimer = Time.unscaledTime;

                    float diff = currentTimer - lastTimer;

                    if (diff <= threshold)
                    {
                        clicks = 0;
                        return true;
                    }
                    else
                    {
                        clicks = 0;
                    }
                }
            }
            return false;
        }
    }
}
