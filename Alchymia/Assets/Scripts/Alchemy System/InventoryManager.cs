﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryManager : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI questionText;
    public Button yesButton;
    public Button noButton;
    public static bool addMode;
    public Animator animator;
    private string question = "Would you like to add this item to your inventory?";
    private GameObject ingredient;
    private SpriteRenderer rend;
    private float f;

    // Start is called before the first frame update
    void Start()
    {
        addMode = false;
    }
    private void Update()
    {
    }

    Dictionary<string, string> RecipeNames = new Dictionary<string, string>()
    {
        {"RedRuby", "Red Ruby" },
        {"OrangeTopaz", "Orange Topaz" },
        {"YellowCitrine", "Yellow Citrine" },
        {"GreenEmerald", "Green Emerald" },
        {"BlueSapphire", "Blue Sapphire" },
        {"PurpleAmethyst", "Purple Amethyst" }
    };

    public void AddItem(GameObject gameObject)
    {
        DialogueManager.dialogueMode = true;
        if (RecipeNames.ContainsKey(gameObject.name)) {
            nameText.text = RecipeNames[gameObject.name];
        } else
        {
            nameText.text = gameObject.name;
        }
        questionText.text = question;
        yesButton.gameObject.SetActive(true);
        if ( ingredient && ingredient != gameObject && rend && rend.color.a!= 1f)
        {
            Debug.Log("voila" + ingredient.name);

            Color c = rend.color;
            c.a = 1f;
            rend.color = c;
            ingredient.GetComponent<Collider2D>().enabled = true;
            ingredient.SetActive(false);
        }
        ingredient = gameObject;
        rend = ingredient.GetComponent<SpriteRenderer>();
        float xPos = GameObject.FindGameObjectWithTag("Protagonist").transform.position.x;
        float yPos = GameObject.FindGameObjectWithTag("Protagonist").transform.position.y;
        yesButton.GetComponent<RectTransform>().anchoredPosition = new Vector3((float)-925, (float)0);
        yesButton.GetComponentInChildren<Text>().text = "Yes";
        noButton.GetComponentInChildren<Text>().text = "No";
        addMode = true;
        animator.SetBool("IsOpen", true);

        SoundManagerScript.PlaySound("dialogue");
        StopAllCoroutines();
        StartCoroutine(TypeQuestion(question));
        
    }

    IEnumerator TypeQuestion(string question)
    {
        questionText.text = "";
        foreach (char letter in question.ToCharArray())
        {
            questionText.text += letter;
            yield return null;
        }
    }

    public void EndAdd(bool add)
    {

        SoundManagerScript.PlaySound("woodButton");
        if (add)
        {
            //Debug.Log("Adding");
            StartCoroutine("FadeItem");
            //ingredient.SetActive(false);
            GameObject newIngredient = Instantiate(ingredient);
            //Debug.Log(newIngredient.name);
            Inventory.AddIngredientItem(newIngredient, Item.Type.Ingredient, newIngredient.GetComponent<SpriteRenderer>().sprite);

        }
        addMode = false;
        animator.SetBool("IsOpen", false);
        //Debug.Log(animator.gameObject.name);
        //Debug.Log("Ended adding.");
        DialogueManager.dialogueMode = false;
    }



    IEnumerator FadeItem()
    {
        //gameobject.GetComponent<Collider2D>().enabled = false;
        ingredient.GetComponent<Collider2D>().enabled = false;
        for(f = 1f; f >= -0.05f; f -= 0.05f)
        {
            Color c = rend.color;
            c.a = f;
            rend.color = c;
            yield return new WaitForSeconds(0.05f);

        }

        if(f < -0.05f)
        {
            //Debug.Log("Here");
            Color c = rend.color;
            c.a = 1f;
            rend.color = c;
            ingredient.GetComponent<Collider2D>().enabled = true;
            ingredient.SetActive(false);
            //ingredient.GetComponent<Collider2D>().enabled = true;
        }
    }
}

