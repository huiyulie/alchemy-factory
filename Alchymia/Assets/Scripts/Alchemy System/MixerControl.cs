﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MixerControl : MonoBehaviour
{
    public static List<PlayerItem> mixerInventory = new List<PlayerItem>();

    //[SerializeField]
    //private GameObject mixerButtonTemplate;
    public GameObject buttonPrefab;

    public GameObject panelToAttachButtonsTo;
    private Transform parent;

    /*
    private void Start()
    {

        GameObject newButton = Instantiate(mixerButtonTemplate) as GameObject;
    }
    public void GenInventory(PlayerItem item)
    {
        if (!mixerButtonTemplate)
        {
            Debug.Log("mixerButtonTemplate is null");
            Debug.Log(mixerButtonTemplate);
            Debug.Log(buttonPrefab);
            Debug.Log(parent);
        }
        else
        {
            Debug.Log("mixerbuttonztemplate is not znull");
            parent = mixerButtonTemplate.transform.parent;
            GameObject newButton = Instantiate(mixerButtonTemplate) as GameObject;
            newButton.SetActive(true);

            newButton.GetComponent<MixerButton>().setIcon(item.iconSprite);
            newButton.transform.SetParent(parent, false);
        }

    }
    */
    public void AddToMixer(Sprite item)
    {
        if(mixerInventory.Count < 3)
        {
            PlayerItem newItem = new PlayerItem();
            newItem.iconSprite = item;
            mixerInventory.Add(newItem);
            /*
            for (int i = 0; i < mixerInventory.Count; i++)
            {
                Debug.Log("aye: " + mixerInventory.Count + ", " + mixerInventory[i].iconSprite.name);
            }
            */
            GenInventory(newItem);
        }
        
        /*
            if (mixerInventory[0].iconSprite)
            {
                mixerInventory[1].iconSprite = item;
                GenInventory(mixerInventory[1]);
            }
            else
            {
                mixerInventory[0].iconSprite = item;
                GenInventory(mixerInventory[0]);
            }
        */
        
    }

    public void ResetMixer()
    {
        SoundManagerScript.PlaySound("reset");
        mixerInventory = new List<PlayerItem>();
        GameObject parent = GameObject.FindGameObjectWithTag("MixContent");
        int num = parent.transform.childCount;
        for (int i = 0; i < num; i++)
        {
            Destroy(parent.transform.GetChild(i).gameObject);
        }
    }

    public void GenInventory(PlayerItem item)
    {

        GameObject go = new GameObject("Textured button (" + item.
            iconSprite.name + ")");

        Image image = go.AddComponent<Image>();
        image.sprite = item.iconSprite;

        UnityEngine.UI.Button button = go.AddComponent<UnityEngine.UI.Button>();

        GameObject parent = GameObject.FindGameObjectWithTag("MixContent");
        go.transform.SetParent(parent.transform, false);



        //image.rectTransform.sizeDelta = size;

        //return button;
    }
    public class PlayerItem
    {
        public Sprite iconSprite;
        public PlayerItem()
        {
            iconSprite = null;
        }
    }
}
