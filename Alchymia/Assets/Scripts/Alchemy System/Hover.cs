﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Hover : MonoBehaviour
{
    public UnityEngine.UI.Text nameText;
    private bool show = false;
    private float Timer;
    public int timeBetweenSpawn = 1;
    private bool countDown = false;
    public static bool hideName = false;

    Dictionary<string, string> RecipeNames = new Dictionary<string, string>()
    {
        {"Medicine", "Medicine" },
        {"Preservative", "Preservative" },
        {"BaitSolution", "Bait Solution" },
        {"Fuel", "Fuel" },
        {"FireExtinguisher", "Fire Extinguisher" },
        {"Fertilizer", "Fertilizer" },
        {"CleaningSolution", "Cleaning Solution" },
        {"EnergyElixir", "Energy Elixir" },
        {"Ointment", "Ointment" },
        {"Pesticide", "Pesticide" },
        {"SpeedPotion", "Speed Potion" },
        {"LovePotion", "Love Potion" },
    };

    void Start()
    {
        nameText.gameObject.SetActive(false);
        Timer = timeBetweenSpawn;
        countDown = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (countDown || hideName)
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0f || hideName)
            {
                countDown = false;
                if (nameText && nameText.gameObject)
                {
                    nameText.gameObject.SetActive(false);
                    hideName = false;
                }
                Timer = timeBetweenSpawn;
            }
        }

    }

    public void toggleName(int index)
    {
        GameObject alchemyItem = Inventory.alchemySlots[index];

        Debug.Log(alchemyItem.GetComponent<Slot>().icon.name);
        if (alchemyItem.GetComponent<Slot>().icon)
        {
            if (RecipeNames.ContainsKey(alchemyItem.GetComponent<Slot>().icon.name))
            {
                nameText.text = RecipeNames[alchemyItem.GetComponent<Slot>().icon.name];
            }
            else
            {
                nameText.text = "";
            }
        } else
        {
            nameText.text = "";
        }
        
        if (!countDown)
        {
            nameText.gameObject.SetActive(true);
            countDown = !countDown;
            Debug.Log("!countdown");
        }
        else
        {
            Debug.Log("else");
        }


    }

    public void removeName()
    {
        nameText.text = "";
    }

    private void OnMouseDown()
    {
        Debug.Log("hello");
    }

    private void OnMouseOver()
    {
        Debug.Log("on mouse over");
        //////if (hitObject = true)
        //////{
        ////    Vector2 screenPos = Input.mousePosition;
        ////    //Vector2 convertedGUIPos = GUIUtility.ScreenToGUIPoint(screenPos);

        ////    GUI.Label(new Rect(screenPos.x, screenPos.y, 200, 20), gameObject.name);
        //////}
        /////
        //text.SetActive(true);
        //Debug.Log("mouse over");
        GameObject nameObject = new GameObject();
        nameObject.AddComponent<TextMeshProUGUI>();
        nameObject.GetComponent<TextMeshProUGUI>().text = gameObject.name;
        Instantiate(nameObject, gameObject.transform.parent);

    }

    private void OnMouseExit()
    {
        //text.SetActive(false);
        Debug.Log("on mouse exit");

    }
}
