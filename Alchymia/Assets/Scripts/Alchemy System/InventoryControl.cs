﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InventoryControl : MonoBehaviour
{

    public Inventory globalInventory;
    private List<PlayerItem> playerInventory;

    [SerializeField]
    private GameObject buttonTemplate;
    [SerializeField]
    private GridLayoutGroup gridGroup;

    [SerializeField]
    private Sprite[] iconSprites;

    void Start()
    {
        
        // GenInventory();
    }

    void resetInv()
    {

        playerInventory = new List<PlayerItem>();
        int num = buttonTemplate.transform.parent.childCount;
        for (int i = 1; i < num; i++)
        {
            Destroy(buttonTemplate.transform.parent.transform.GetChild(i).gameObject);
        }
       
    }
    public void GenInventory()
    {
        resetInv();
        //Debug.Log("REEEEEEEEEE: " + Inventory.slots.Count);
        for (int i = 0; i < Inventory.ingredientSlots.Count; i++)
        {
            PlayerItem newItem = new PlayerItem();
            //newItem.iconSprite = iconSprites[Random.Range(0, iconSprites.Length)];
            if (Inventory.ingredientSlots[i].GetComponent<Slot>().empty == false)
            {
                newItem.iconSprite = Inventory.ingredientSlots[i].GetComponent<Slot>().icon;
                //Debug.Log(newItem.iconSprite.name);

                playerInventory.Add(newItem);
            }

        }
        if (playerInventory.Count < 5)
        {
            gridGroup.constraintCount = playerInventory.Count;
        }
        else
        {
            gridGroup.constraintCount = 6;
        }
        foreach (PlayerItem newItem in playerInventory)
        {
            GameObject newButton = Instantiate(buttonTemplate) as GameObject;
            newButton.SetActive(true);

            newButton.GetComponent<InventoryButton>().setIcon(newItem.iconSprite);
            newButton.transform.SetParent(buttonTemplate.transform.parent, false);
  
        }
        
    }


    public class PlayerItem
    {
        public Sprite iconSprite;

        public PlayerItem()
        {
            iconSprite = null;
        }
    }
}
