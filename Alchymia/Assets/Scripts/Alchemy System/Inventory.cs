﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Inventory : MonoBehaviour
{
    public static bool inventoryEnabled = false;
    public GameObject inventory;
    public static int allSlots;
    public static int ingredientsIndex = 0;
    public static int alchemyIndex = 0;
    public static List<GameObject> ingredientSlots;
    public static List<GameObject> alchemySlots;
    public GameObject ingredientPanel;
    public GameObject alchemyPanel;
    public GameObject ingredientSlotHolder;
    public GameObject alchemySlotHolder;
    public Sprite emptySlot;
    public bool ingredients = true;
    public SaveData saveInstance;
    // Start is called before the first frame update
    void Start()
    {
        ingredientSlots = new List<GameObject>();
        alchemySlots = new List<GameObject>();
        allSlots = 50;
        ingredientsIndex = 0;
        alchemyIndex = 0;
        for (int i = 0; i < allSlots; i++)
        {
            ingredientSlots.Add(ingredientSlotHolder.transform.GetChild(i).gameObject);
            if (ingredientSlots[i].GetComponent<Slot>().item == null)
            {
                ingredientSlots[i].GetComponent<Slot>().empty = true;
            }

        }
        for (int i = 0; i < allSlots; i++)
        {
            alchemySlots.Add(alchemySlotHolder.transform.GetChild(i).gameObject);
            if (alchemySlots[i].GetComponent<Slot>().item == null)
            {
                alchemySlots[i].GetComponent<Slot>().empty = true;
            }

        }
        inventory.SetActive(false);
        ingredientPanel.SetActive(false);
        alchemyPanel.SetActive(false);


        saveInstance = new SaveData();
        saveInstance.setInventory();
    }

    // Update is called once per frame
    void Update()
    {
        if (inventoryEnabled)
        {
            inventory.SetActive(true);
            if (ingredients)
            {
                ingredientPanel.SetActive(true);
                alchemyPanel.SetActive(false);
            }
            else
            {
                ingredientPanel.SetActive(false);
                alchemyPanel.SetActive(true);
            }

        }
        else
        {
            inventory.SetActive(false);
            ingredientPanel.SetActive(false);
            alchemyPanel.SetActive(false);
        }
    }

    public void CloseBag()
    {
        inventoryEnabled = false;

        SoundManagerScript.PlaySound("bag");

        GameObject.FindGameObjectWithTag("Protagonist").GetComponent<NavMeshAgent>().destination = GameObject.FindGameObjectWithTag("Protagonist").GetComponent<NavMeshAgent>().transform.position;
        GameObject.FindGameObjectWithTag("Protagonist").GetComponent<NavMeshAgent>().isStopped = false;
        DialogueManager.dialogueMode = false;
    }

    public static void AddIngredientItem(GameObject item, Item.Type type, Sprite icon)
    {
        //allSlots = 50;
        for (int i = 0; i < allSlots; i++)
        {
            if (ingredientSlots[i].GetComponent<Slot>().empty)
            {
                //Debug.Log("INV SPRITE" + item.GetComponent<SpriteRenderer>().sprite.name + ", " + icon.name);
                ingredientSlots[i].GetComponent<Slot>().item = item;
                ingredientSlots[i].GetComponent<Slot>().icon = icon;
                ingredientSlots[i].GetComponent<Slot>().type = type;
                ingredientSlots[i].GetComponent<Slot>().item.name = icon.name + "(Clone)";
                item.transform.parent = ingredientSlots[i].transform;
                item.SetActive(false);
                ingredientSlots[i].GetComponent<Slot>().UpdateSlot();
                ingredientSlots[i].GetComponent<Slot>().empty = false;
                break;
            }
        }
        ingredientsIndex++;
    }

    public void RemoveItemByName(string item)
    {
        //int targetIndex = -1;
        for (int i = 0; i < ingredientSlots.Count; i++)
        {
            /*
            if (ingredientSlots[i].GetComponent<Slot>().item != null)
            {

                Debug.Log("inv -> " + ingredientSlots[i].GetComponent<Slot>().item.name);
            }
            */

            if ((ingredientSlots[i].GetComponent<Slot>().item != null) && (ingredientSlots[i].GetComponent<Slot>().item.name == (item + "(Clone)")))
            {
                //targetIndex = i;

                //Debug.Log("inv in -> " + ingredientSlots[i].GetComponent<Slot>().item.name + " ---- " + item + "(Clone)");
                RemoveIngredientItem(i);
                break;
            }
        }
        //RemoveItem(targetIndex);
    }

    public void RemoveIngredientItem(int ind)
    {
        if (ingredientSlots[ind].GetComponent<Slot>().item == null || ingredientSlots[ind].GetComponent<Slot>().empty)
        {
            return;
        }
        Debug.Log("removing item");
        Debug.Log("ind: " + ind);
        Debug.Log("ingredientsIndex" + ingredientsIndex);
        ingredientsIndex--;
        for (int i = ind; i <= ingredientsIndex; i++)
        {
            //Debug.Log("i: " + i);
            if (i == ingredientsIndex)
            {
                ingredientSlots[i].GetComponent<Slot>().icon = emptySlot;
                ingredientSlots[i].GetComponent<Slot>().empty = true;
                ingredientSlots[i].GetComponent<Slot>().UpdateSlot();
                continue;
            }
            ingredientSlots[i].GetComponent<Slot>().item = ingredientSlots[i + 1].GetComponent<Slot>().item;
            ingredientSlots[i].GetComponent<Slot>().icon = ingredientSlots[i + 1].GetComponent<Slot>().icon;
            ingredientSlots[i].GetComponent<Slot>().type = ingredientSlots[i + 1].GetComponent<Slot>().type;
            ingredientSlots[i + 1].GetComponent<Slot>().item.transform.parent = ingredientSlots[i].transform;
            ingredientSlots[i + 1].GetComponent<Slot>().item.SetActive(false);
            ingredientSlots[i].GetComponent<Slot>().UpdateSlot();
            ingredientSlots[i].GetComponent<Slot>().empty = ingredientSlots[i + 1].GetComponent<Slot>().empty;
        }
    }

    public static void AddAlchemyItem(GameObject item, Item.Type type, Sprite icon)
    {
        //Debug.Log("alchemy icon's name is:" + icon.name);
        //allSlots = 50;
        for (int i = 0; i < allSlots; i++)
        {
            if (alchemySlots[i].GetComponent<Slot>().empty)
            {
                Debug.Log(item.name);
                alchemySlots[i].GetComponent<Slot>().item = item;
                Debug.Log(alchemySlots[i].GetComponent<Slot>().item.name);
                alchemySlots[i].GetComponent<Slot>().icon = icon;
                alchemySlots[i].GetComponent<Slot>().type = type;
                item.transform.parent = alchemySlots[i].transform;
                item.SetActive(false);
                alchemySlots[i].GetComponent<Slot>().UpdateSlot();
                alchemySlots[i].GetComponent<Slot>().empty = false;

                break;
            }
        }
        alchemyIndex++;
        //Debug.Log("alchemyIndex is :" + alchemyIndex);
    }
    public void RemoveAlchemyItemByName(string item)
    {
        //int targetIndex = -1;
        for (int i = 0; i < alchemySlots.Count; i++)
        {
            if ((alchemySlots[i].GetComponent<Slot>().item != null) && (alchemySlots[i].GetComponent<Slot>().item.name == (item + "(Clone)")))
            {
                //targetIndex = i;
                RemoveAlchemyItem(i);
                break;
            }
        }
        //RemoveItem(targetIndex);
    }
    public void RemoveAlchemyItem(int ind)
    {
        if (alchemySlots[ind].GetComponent<Slot>().item == null || alchemySlots[ind].GetComponent<Slot>().empty)
        {
            return;
        }
        Debug.Log("removing item");
        Debug.Log("ind: " + ind);
        Debug.Log("alchemysIndex" + alchemyIndex);
        alchemyIndex--;
        for (int i = ind; i <= alchemyIndex; i++)
        {
            Debug.Log("i: " + i);
            if (i == alchemyIndex)
            {
                alchemySlots[i].GetComponent<Slot>().icon = emptySlot;
                alchemySlots[i].GetComponent<Slot>().empty = true;
                alchemySlots[i].GetComponent<Slot>().UpdateSlot();
                alchemySlots[i].GetComponent<Slot>().removeName();
                Hover.hideName = true;
                continue;
            }
            alchemySlots[i].GetComponent<Slot>().item = alchemySlots[i + 1].GetComponent<Slot>().item;
            alchemySlots[i].GetComponent<Slot>().icon = alchemySlots[i + 1].GetComponent<Slot>().icon;
            alchemySlots[i].GetComponent<Slot>().type = alchemySlots[i + 1].GetComponent<Slot>().type;
            alchemySlots[i + 1].GetComponent<Slot>().item.transform.parent = alchemySlots[i].transform;
            alchemySlots[i + 1].GetComponent<Slot>().item.SetActive(false);
            alchemySlots[i].GetComponent<Slot>().UpdateSlot();
            alchemySlots[i].GetComponent<Slot>().empty = alchemySlots[i + 1].GetComponent<Slot>().empty;
        }
    }

    public void IngredientItems()
    {
        ingredients = true;

        SoundManagerScript.PlaySound("bag");
    }

    public void AlchemyItems()
    {
        ingredients = false;

        SoundManagerScript.PlaySound("bag");
    }
}

