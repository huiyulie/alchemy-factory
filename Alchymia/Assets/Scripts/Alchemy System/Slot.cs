﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public GameObject item;
    public bool empty = true;
    public Sprite icon;

    public Item.Type type;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateSlot()
    {
        this.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = icon;
    }
    public void OnMouseDown()
    {
        Debug.Log("hello");
    }
    public void removeName()
    {
        item.name = "";
    }
}
