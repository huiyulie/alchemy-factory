﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class AddItemToInventory : MonoBehaviour
{
    NavMeshObstacle obstacle;

    // Start is called before the first frame update
    void Start()
    {
        //obstacle = GetComponent<NavMeshObstacle>();
        //obstacle.enabled = true;
        //obstacle.carving = true;
        //Debug.Log("obstacle");
    }
    public void OnMouseDown()
    {

        SoundManagerScript.PlaySound("moveClick");
        //Debug.Log("MouseDown detected");
        if (InventoryManager.addMode == false)
        {
            FindObjectOfType<InventoryManager>().AddItem(gameObject);
        }
    }

}

//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class AddItemToBackpack : MonoBehaviour
//{
//    //Just overlapped a collider 2D
//    private void OnCollisionEnter2D(Collision2D col)
//    {
//        Debug.Log("On Collision");
//        if (col.gameObject.CompareTag("Strawberry Jam"))
//        {
//            Debug.Log("Strawberry Jam");
//            GameObject.FindWithTag("Strawberry Jam").SetActive(false);
//            GameObject.Find("Strawberry Jam Inside Bag").tag = "Inside Bag";
//            // col.gameObject.GetComponent<Renderer>().enabled = false;
//        }
//    }

//    // Start is called before the first frame update
//    void Start()
//    {
//        GameObject.FindWithTag("Inside Bag").GetComponent<Renderer>().enabled = false;
//        GameObject.FindWithTag("Strawberry Jam Inside Bag").GetComponent<Renderer>().enabled = false;
//    }

//    // Update is called once per frame
//    void Update()
//    {

//    }

//}