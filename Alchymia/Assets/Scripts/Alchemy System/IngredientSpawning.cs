﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IngredientSpawning : MonoBehaviour
{
    private int randomIndex;
    private int randomNumber;
    private float Timer;
    public int timeBetweenSpawn;
    public int numberOfSpawned = 12;
    public GameObject[] ingredients;
    private GameObject[] spawnedIngredients;
    public static bool spawn = true;
    // Start is called before the first frame update
    void Start()
    {
        IngredientSpawn();
        Timer = timeBetweenSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (spawn && !(DialogueManager.dialogueMode == true))
        {
            for (int i = 0; i < ingredients.Length; i++)
            {
                ingredients[i].transform.position = ingredients[i].transform.position + (float)0.005 * Vector3.up * Mathf.Cos(Time.time + i);
            }

            Timer -= Time.deltaTime;
            if (Timer <= 0f)
            {
                IngredientSpawn();
                Timer = timeBetweenSpawn;
            }
        }
    }

    void IngredientSpawn()
    {
        List<int> indices = new List<int>();
        for (int i = 0; i < ingredients.Length; i++)
        {
            if (ingredients[i].CompareTag("Fruit"))
            {
                ingredients[i].transform.localScale = new Vector3((float)0.5, (float)0.5);
            }
            else if (ingredients[i].CompareTag("Jewel"))
            {
                ingredients[i].transform.localScale = new Vector3((float)0.25, (float)0.25);
            }
        }
        //randomNumber = Random.Range(1, ingredients.Length);
        randomNumber = numberOfSpawned;
        for (int i = 0; i < randomNumber; i++)
        {
            randomIndex = Random.Range(0, ingredients.Length);
            if (indices.Contains(randomIndex))
            {
                i--;
                continue;
            }
            indices.Add(randomIndex);
            Debug.Log(randomIndex);
        }
        for (int i = 0; i < ingredients.Length; i++)
        {
            if (indices.Contains(i))
            {
                Vector3 protagonistPos = GameObject.FindGameObjectWithTag("Protagonist").transform.position;
                //Debug.Log("xPos: " + xPos);
                float yPos = GameObject.FindGameObjectWithTag("Protagonist").transform.position.y;
                //Debug.Log("yPos: " + yPos);
                if (ingredients[i].name == "Strawberry" || ingredients[i].name == "RedRuby")
                {
                    ingredients[i].transform.position = new Vector3(Random.Range(-20f, -11f), Random.Range(-35f, 30f), -1);
                }
                else if (ingredients[i].name == "Orange" || ingredients[i].name == "OrangeTopaz")
                {
                    ingredients[i].transform.position = new Vector3(Random.Range(-11f, -2f), Random.Range(-35f, 30f), -1);
                }
                else if (ingredients[i].name == "Banana" || ingredients[i].name == "YellowCitrine")
                {
                    ingredients[i].transform.position = new Vector3(Random.Range(-2f, 7f), Random.Range(-35f, 30f), -1);
                }
                else if (ingredients[i].name == "Pear" || ingredients[i].name == "GreenEmerald")
                {
                    ingredients[i].transform.position = new Vector3(Random.Range(7f, 16f), Random.Range(-35f, 30f), -1);
                }
                else if (ingredients[i].name == "Blueberry" || ingredients[i].name == "BlueSapphire")
                {
                    ingredients[i].transform.position = new Vector3(Random.Range(16f, 25f), Random.Range(-35f, 30f), -1);
                }
                else if (ingredients[i].name == "Grapes" || ingredients[i].name == "PurpleAmethyst")
                {
                    ingredients[i].transform.position = new Vector3(Random.Range(25f, 34f), Random.Range(-35f, 30f), -1);
                }
                if ((ingredients[i].transform.position - protagonistPos).sqrMagnitude < 0.1) {
                    Debug.Log("FALSE");
                    ingredients[i].SetActive(false);
                    continue;
                }
                NavMeshHit hit;
                Vector3 comparePos = ingredients[i].transform.position + new Vector3(0, 0, 1);
                while (!NavMesh.SamplePosition(comparePos, out hit, (float)0.1, NavMesh.AllAreas))
                {
                    /*Debug.Log("x");
                    Debug.Log(ingredients[i].transform.position.x);
                    Debug.Log(hit.position.x);
                    Debug.Log("y");
                    Debug.Log(ingredients[i].transform.position.y);
                    Debug.Log(hit.position.y);
                    Debug.Log("z");
                    Debug.Log(ingredients[i].transform.position.z);
                    Debug.Log(hit.position.z);*/
                    // Check if the positions are vertically aligned
                    if (ingredients[i].name == "Strawberry" || ingredients[i].name == "RedRuby")
                    {
                        ingredients[i].transform.position = new Vector3(Random.Range(-20f, -11f), Random.Range(-35f, 30f), -1);
                    }
                    else if (ingredients[i].name == "Orange" || ingredients[i].name == "OrangeTopaz")
                    {
                        ingredients[i].transform.position = new Vector3(Random.Range(-11f, -2f), Random.Range(-35f, 30f), -1);
                    }
                    else if (ingredients[i].name == "Banana" || ingredients[i].name == "YellowCitrine")
                    {
                        ingredients[i].transform.position = new Vector3(Random.Range(-2f, 7f), Random.Range(-35f, 30f), -1);
                    }
                    else if (ingredients[i].name == "Pear" || ingredients[i].name == "GreenEmerald")
                    {
                        ingredients[i].transform.position = new Vector3(Random.Range(7f, 16f), Random.Range(-35f, 30f), -1);
                    }
                    else if (ingredients[i].name == "Blueberry" || ingredients[i].name == "BlueSapphire")
                    {
                        ingredients[i].transform.position = new Vector3(Random.Range(16f, 25f), Random.Range(-35f, 30f), -1);
                    }
                    else if (ingredients[i].name == "Grapes" || ingredients[i].name == "PurpleAmethyst")
                    {
                        ingredients[i].transform.position = new Vector3(Random.Range(25f, 34f), Random.Range(-35f, 30f), -1);
                    }
                    if ((ingredients[i].transform.position - protagonistPos).sqrMagnitude < 0.1)
                    {
                        Debug.Log("FALSE");
                        ingredients[i].SetActive(false);
                        continue;
                    }
                    comparePos = ingredients[i].transform.position + new Vector3(0, 0, 1);
                    if (Mathf.Approximately(ingredients[i].transform.position.x, hit.position.x)
                        && Mathf.Approximately(ingredients[i].transform.position.z, hit.position.z - 1))
                    {
                        // Lastly, check if object is below navmesh
                        if (ingredients[i].transform.position.y < hit.position.y)
                        {
                            Debug.Log("FALSE2");
                            ingredients[i].SetActive(false);
                            continue;
                        }
                    }
                }
                    //else
                //{
                //    while (
                //}
                bool setFalse = false;
                for (int j = 0; j < ingredients.Length; j++)
                {
                    if (ingredients[j].activeSelf && ingredients[j] != ingredients[i])
                    {
                        if (Vector3.Distance(ingredients[j].transform.position, ingredients[i].transform.position) < 1f)
                        {
                            Debug.Log("FALSE4");
                            ingredients[i].SetActive(false);
                            setFalse = true;
                            break;
                        }
                    }
                }
                if (!setFalse)
                {
                    ingredients[i].SetActive(true);
                }
            }
            else
            {
                Debug.Log("FALSE5");
                ingredients[i].SetActive(false);
            }
        }
    }
}
