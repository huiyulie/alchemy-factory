﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour
{
    [SerializeField]
    private Image myIcon;
    public MixerControl mixCon;

    public void Start()
    {
        mixCon = new MixerControl();
    }

    public void setIcon(Sprite mySprite)
    {
        myIcon.sprite = mySprite;
    }

    public void MoveOnClick()
    {
        //Debug.Log("in MoveOnClick");
        //Debug.Log("hereeee" + myIcon.sprite.name);
        if(MixerControl.mixerInventory.Count < 3)
        {
            SoundManagerScript.PlaySound("moveClick");
            mixCon.AddToMixer(myIcon.sprite);
            this.transform.SetParent(null, false);
        }
        //MixerControl.AddToMixer(myIcon.sprite);
        

    }

    public Sprite getIcon()
    {
        return myIcon.sprite;
    }
}
