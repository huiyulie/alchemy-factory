﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Addition : MonoBehaviour
{
    public GameObject items;
    public GameObject result;
    public GameObject particle;

    bool item_1 = false;
    bool item_2 = false;
    bool item1_in = false;
    bool item2_in = false;
    // Start  before the first frame update
    void Start()
    {
        //result.SetActive(false);
        //particle.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("Mouse Down Event");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.transform.name);
                if (hit.transform.name == "Item_1")
                {
                    Debug.Log("This is Item_1");
                    item_1 = true;
                    item_2 = false;
                    
                }
                else if ((item_1 == true) && (hit.transform.name == "Cauldron_holder"))
                {
                    Debug.Log("Item_1 in!");
                    item1_in = true;
                    items.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Renderer>().material.color -= new Color(0,0,0,0.7f);
                    //items.transform.GetChild(0).gameObject.SetActive(false);
                    if (item2_in)
                    {
                        StartCoroutine(Mix());
                    }
                }
                else if(hit.transform.name == "Item_2")
                {
                    Debug.Log("This is Item_2");
                    item_2 = true;
                    item_1 = false;
                }
                else if ((item_2 == true) && (hit.transform.name == "Cauldron_holder"))
                {
                    Debug.Log("Item_2 in!");
                    item2_in = true;
                    //items.transform.GetChild(1).gameObject.SetActive(false);
                    items.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Renderer>().material.color -= new Color(0, 0, 0, 0.7f);
                    if (item1_in)
                    {
                        Debug.Log("Bro please");
                        StartCoroutine(Mix());
                    }
                }

            }
        }

        IEnumerator Mix()
        {
            Debug.Log("Yeah");
            particle.SetActive(true);
            yield return new WaitForSeconds(5);
            particle.SetActive(false);
            result.SetActive(true);
        }
        
    }
}