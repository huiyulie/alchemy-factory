﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CauldronControl : MonoBehaviour
{

    public Inventory globalInventory;
    private GameObject ingredient;
    public Button cauldronButton;

    [SerializeField]
    private Sprite[] recipeSprites;

    public static Sprite final_result;

    [SerializeField]
    private GameObject buttonTemplate;

    public GameObject recipeListContent;

    //Dictionary<string, List<string>> Recipes = new Dictionary<string, List<string>>();
    Dictionary<string, List<string>> Recipes = new Dictionary<string, List<string>>()
    {
        {"Medicine", new List<string> { "Strawberry", "OrangeTopaz" } },
        {"Preservative", new List<string> { "Orange","YellowCitrine" } },
        {"BaitSolution", new List<string> { "Banana","GreenEmerald" } },
        {"Fuel", new List<string> { "Pear", "BlueSapphire" } },
        {"FireExtinguisher", new List<string> { "Blueberry","PurpleAmethyst" } },
        {"Fertilizer", new List<string> { "Orange","RedRuby" } },
        {"CleaningSolution", new List<string> { "Banana", "OrangeTopaz" } },
        {"EnergyElixir", new List<string> { "Pear","YellowCitrine" } },
        {"Ointment", new List<string> { "Blueberry","GreenEmerald" } },
        {"Pesticide", new List<string> { "Grapes", "BlueSapphire" } },
        {"OrangeTopaz", new List<string> { "RedRuby", "YellowCitrine" } },
        {"YellowCitrine", new List<string> { "RedRuby", "GreenEmerald" } },
        {"PurpleAmethyst", new List<string> { "RedRuby", "BlueSapphire" } }
    };

    Dictionary<string, string> RecipeNames = new Dictionary<string, string>()
    {
        {"Medicine", "Medicine" },
        {"Preservative", "Preservative" },
        {"BaitSolution", "Bait Solution" },
        {"Fuel", "Fuel" },
        {"FireExtinguisher", "Fire Extinguisher" },
        {"Fertilizer", "Fertilizer" },
        {"CleaningSolution", "Cleaning Solution" },
        {"EnergyElixir", "Energy Elixir" },
        {"Ointment", "Ointment" },
        {"Pesticide", "Pesticide" },
        {"OrangeTopaz", "Orange Topaz" },
        {"YellowCitrine", "Yellow Citrine" },
        {"PurpleAmethyst", "Purple Amethyst" }
    };

    Dictionary<string, List<string>> ComplexRecipes = new Dictionary<string, List<string>>()
    {
        {"LovePotion", new List<string> { "Strawberry", "RedRuby", "Grapes" } },
        {"SpeedPotion", new List<string> { "Blueberry", "BlueSapphire", "PurpleAmethyst"} }
    };

    Dictionary<string, string> ComplexRecipesNames = new Dictionary<string, string>()
    {
        {"LovePotion", "Love Potion" },
        {"SpeedPotion", "Speed Potion" }
    };

    // Start is called before the first frame update
    void Start()
    {
        Button btn = cauldronButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);



    }

    void TaskOnClick()
    {
        Debug.Log("TIME TO MIX");

        Transform[] ts = recipeListContent.transform.GetComponentsInChildren<Transform>(true);

        /*
        foreach (Transform t in ts)
        {
            foreach (Sprite sp in recipeSprites)
            {
                if (t.gameObject.name == sp.name)
                {
                    Debug.Log("FOUND NEW RECIPE POG -->" + t.gameObject.name);

                    t.gameObject.SetActive(true);
                }
            }
        }
        */

        GameObject Mixer = GameObject.FindGameObjectWithTag("MixContent");
        Debug.Log("CHILDREN HERE: " + Mixer.transform.childCount);
        if (Mixer.transform.childCount == 2)
        {
            GameObject btn1 = Mixer.transform.GetChild(0).gameObject;
            GameObject btn2 = Mixer.transform.GetChild(1).gameObject;

            Image img1 = btn1.GetComponent<Image>();
            Image img2 = btn2.GetComponent<Image>();

            Sprite sp1 = img1.sprite;
            Sprite sp2 = img2.sprite;

            string result = "";

            foreach (KeyValuePair<string, List<string>> entry in Recipes)
            {
                if ((sp1.name == entry.Value[0] && sp2.name == entry.Value[1]) || (sp2.name == entry.Value[0] && sp1.name == entry.Value[1]))
                {
                    result = entry.Key;
                }
            }

            for (int i = 0; i < recipeSprites.Length; i++)
            {
                if (recipeSprites[i].name == result)
                {
                    final_result = recipeSprites[i];
                }
            }

            if (result != "")
            {
                foreach (Transform t in ts)
                {
                    //Debug.Log("COMPARE: " + t.gameObject.name + " " + final_result.name);
                    if (t.gameObject.name == final_result.name)
                    {
                        Debug.Log("FOUND NEW RECIPE POG -->" + t.gameObject.name);

                        t.gameObject.SetActive(true);
                    }
                }
            }


            Destroy(btn1);
            Destroy(btn2);

            //Remove these items from the Global Inventory
            //globalInventory.RemoveItem()
            globalInventory.RemoveItemByName(sp1.name);
            globalInventory.RemoveItemByName(sp2.name);

            if (result != "")
            {

                SoundManagerScript.PlaySound("right");
                GameObject go = new GameObject("Textured button (" + final_result.name + ")");

                Image image = go.AddComponent<Image>();
                image.sprite = final_result;

                UnityEngine.UI.Button button = go.AddComponent<UnityEngine.UI.Button>();

                button.onClick.AddListener(addBackToInventory);

                go.transform.SetParent(Mixer.transform, false);


                GameObject goTxt = new GameObject("Label (" + final_result.name + ")");
                goTxt.transform.SetParent(Mixer.transform, false);
                Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
                UnityEngine.UI.Text txt = goTxt.AddComponent<UnityEngine.UI.Text>();
                //txt.text = final_result.name;
                txt.text = RecipeNames[final_result.name];
                txt.color = new Color32(240, 139, 71, 255);
                txt.font = ArialFont;
                txt.alignment = TextAnchor.MiddleCenter;
                txt.fontSize = 16;
                txt.horizontalOverflow = HorizontalWrapMode.Overflow;

            }
            else
            {

                SoundManagerScript.PlaySound("wrong");
                MixerControl.mixerInventory.Clear();
            }

        }
        else if (Mixer.transform.childCount == 3)
        {
            Debug.Log("In the TRIPLE TEASER");
            GameObject btn1 = Mixer.transform.GetChild(0).gameObject;
            GameObject btn2 = Mixer.transform.GetChild(1).gameObject;
            GameObject btn3 = Mixer.transform.GetChild(2).gameObject;

            Image img1 = btn1.GetComponent<Image>();
            Image img2 = btn2.GetComponent<Image>();
            Image img3 = btn3.GetComponent<Image>();

            Sprite sp1 = img1.sprite;
            Sprite sp2 = img2.sprite;
            Sprite sp3 = img3.sprite;

            string result = "";

            foreach (KeyValuePair<string, List<string>> entry in ComplexRecipes)
            {
                List<string> allSeen = new List<string>(entry.Value);
                if (allSeen.Contains(sp1.name))
                {
                    allSeen.Remove(sp1.name);
                }
                if (allSeen.Contains(sp2.name))
                {
                    allSeen.Remove(sp2.name);
                }
                if (allSeen.Contains(sp3.name))
                {
                    allSeen.Remove(sp3.name);
                }
                if (allSeen.Count == 0)
                {
                    result = entry.Key;
                    Debug.Log("ALL GONE WOOHOO");
                }
            }

            for (int i = 0; i < recipeSprites.Length; i++)
            {
                if (recipeSprites[i].name == result)
                {
                    final_result = recipeSprites[i];
                }
            }

            Destroy(btn1);
            Destroy(btn2);
            Destroy(btn3);

            //Remove these items from the Global Inventory
            //globalInventory.RemoveItem()
            globalInventory.RemoveItemByName(sp1.name);
            globalInventory.RemoveItemByName(sp2.name);
            globalInventory.RemoveItemByName(sp3.name);

            if (result != "")
            {

                SoundManagerScript.PlaySound("right");
                GameObject go = new GameObject("Textured button (" + final_result.name + ")");

                Image image = go.AddComponent<Image>();
                image.sprite = final_result;

                UnityEngine.UI.Button button = go.AddComponent<UnityEngine.UI.Button>();

                button.onClick.AddListener(addBackToInventory);

                go.transform.SetParent(Mixer.transform, false);

                GameObject goTxt = new GameObject("Label (" + final_result.name + ")");
                goTxt.transform.SetParent(Mixer.transform, false);
                Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
                UnityEngine.UI.Text txt = goTxt.AddComponent<UnityEngine.UI.Text>();
                //txt.text = final_result.name;
                txt.text = ComplexRecipesNames[final_result.name];
                txt.color = new Color32(240, 139, 71, 255);
                txt.font = ArialFont;
                txt.alignment = TextAnchor.MiddleCenter;
                txt.fontSize = 16;
                txt.horizontalOverflow = HorizontalWrapMode.Overflow;
            }
            else
            {

                SoundManagerScript.PlaySound("wrong");
            }

        }
        else
        {
            Debug.Log("Please Select two or three ingredients");
        }

    }

    void addBackToInventory()
    {

        SoundManagerScript.PlaySound("retAlc");
        GameObject newButton = Instantiate(buttonTemplate) as GameObject;
        newButton.SetActive(true);

        newButton.GetComponent<InventoryButton>().setIcon(final_result);
        newButton.transform.SetParent(buttonTemplate.transform.parent, false);

        GameObject Mixer = GameObject.FindGameObjectWithTag("MixContent");
        GameObject btn1 = Mixer.transform.GetChild(0).gameObject;
        Destroy(btn1);
        GameObject gObj = new GameObject();
        gObj.AddComponent<SpriteRenderer>().sprite = final_result;
        
        //gObj.GetComponent<SpriteRenderer>().sprite = final_result;

        bool raw_item = false;

        List<string> gemList = new List<string>();
        gemList.Add("PurpleAmethyst");
        gemList.Add("YellowCitrine");
        gemList.Add("OrangeTopaz");


        foreach (string gemName in gemList)
        {
            if (final_result.name == gemName)
            {
                raw_item = true;
            }
        }
        if (raw_item)
        {
            Inventory.AddIngredientItem(gObj, Item.Type.Ingredient, final_result);
        }
        else
        {
            Inventory.AddAlchemyItem(gObj, Item.Type.Alchemy, final_result);
        }

        //Updating Things
        this.transform.parent.transform.parent.transform.GetChild(2).GetComponent<MixerControl>().ResetMixer();
        this.transform.parent.transform.parent.transform.GetChild(1).GetComponent<InventoryControl>().GenInventory();

    }

    // Update is called once per frame
    void Update()
    {

    }
}
