﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class EndDialogueManager : MonoBehaviour
{
    private List<string> sentences;
    private List<string> names;
    public TextMeshProUGUI dialogueText;
    public TextMeshProUGUI dialogueName;
    public Animator animator;
    private int currCount = 0;
    private bool sentenceDone = false;

    void Start()
    {
        names = new List<string>()
        {
            "Teorian Docere: ",
            "Exciere Coquus: ",
            "Eskhara Visser: ",
            "Nervus Firmarius: ",
            "Eupherine Taliare: ",
            "Teorian Docere: "
        };
            sentences = new List<string>()
        {
            "Hi there! Don’t be scared. We’re all here to officially welcome you into our community...",
            "You’ve been a blessing in disguise and have brought so much happiness to everyone...",
            "You’ve helped us all build confidence in ourselves and do our jobs better...",
            "Y... Y... You’ve always been there for us and helped us in our time of need...",
            "we would all love for you to move in here permanently and be a part of our family...",
            "Congratulations! You are our town's official alchemist! "
        };
        Invoke("StartDialogue", 1);
    }
    public void StartDialogue()
    {
        animator.SetBool("isOpen", true);
        currCount = 0;

        //DisplayNextSentence();
        Invoke("DisplayNextSentence", 1);
    }

    public void DisplayNextSentence()
    {
        
        currCount += 1;
        sentenceDone = false;
        dialogueName.text = names[currCount];
        string sentence = sentences[currCount];
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
        //sentenceDone = true;
    }
    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
        sentenceDone = true;
        Debug.Log("s2: " + sentenceDone);
        if (currCount == sentences.Count - 1)
        {
            StartConfetti();
            Invoke("EndDialogue", 3);
            //EndDialogue();
        }
        else
        {
            Invoke("DisplayNextSentence", 2);
            //DisplayNextSentence();
        }
    }

    public void EndDialogue()
    {

        //SoundManagerScript.PlaySound("dialogue");
        animator.SetBool("isOpen", false);

        Debug.Log("End of Conversation.");
        sentenceDone = false;
    }
    public void StartConfetti()
    {
        //Call confetti
        Window_Confetti_End.startConfetti = true;
        Invoke("GoToMenu", 10);
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
