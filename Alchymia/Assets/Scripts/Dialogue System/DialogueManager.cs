﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    private List<string> sentences;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;
    public Button dialogueButton;
    public Button prevDialogueButton;
    public GameObject[] hearts;
    public static bool dialogueMode;
    public static bool dialogueStarted;
    public Animator animator;
    private int diaLen = 0;
    private int currCount = 0;
    private bool giftreceived = false;
    public Inventory globalInv;
    private int giftSlot;
    public EndSceneCaller endcaller;
    private bool callEndScene = false;

    public GameObject heartTemplate;

    /*
    Dictionary<string, string> Gifts = new Dictionary<string, string>()
    {
        {"Teorian Doc", "Medicine"},
        //{"Saed Grossus", "Preservative"},
        {"Eskhara Visser", "BaitSolution"},
        //{"Angra Farrier", "Fuel"},
        //{"Hap Baecere", "FireExtinguisher"},
        {"Nervus Firmarius", "Fertilizer"},
        {"Exciere Coquus", "CleaningSolution"},
        //{"Jol Meir", "EnergyElixir"},
        {"Eupherine Taliare", "Ointment"},
        //{"Glaed Fleuriste", "Pesticide"}
    };
    */
    Dictionary<string, List<string>> Gifts = new Dictionary<string, List<string>>()
    {
        {"Teorian Doc", new List<string> { "Medicine", "EnergyElixir", "Ointment" } },
        {"Eskhara Visser", new List<string> { "BaitSolution", "Fuel", "FireExtinguisher" } },
        {"Nervus Firmarius", new List<string> { "Fertilizer", "Preservative", "LovePotion" } },
        {"Exciere Coquus", new List<string> { "CleaningSolution", "FireExtinguisher", "Preservative" } },
        {"Eupherine Taliare", new List<string> { "Ointment", "Pesticide", "SpeedPotion" } }
    };

    // Start is called before the first frame update
    void Start()
    {
        sentences = new List<string>();
        dialogueMode = false;
        dialogueStarted = false;
        prevDialogueButton.gameObject.SetActive(false);
        //hearts.gameObject.SetActive(false);
    }
    public void StartDialogue(Dialogue dialogue, NPC npcObj)
    {

        dialogueButton.GetComponentInChildren<Text>().text = "Next >>";
        dialogueMode = true;
        dialogueStarted = true;
        animator.SetBool("IsOpen", true);
        SoundManagerScript.PlaySound("dialogue");
        Debug.Log("Starting conversation with " + dialogue.name);
        nameText.text = dialogue.name;

        sentences.Clear();
        prevDialogueButton.gameObject.SetActive(false);
        currCount = 0;
        callEndScene = false;

        foreach (GameObject heart in hearts)
        {
            heart.SetActive(false);
        }

        if (npcObj.dialogue_field == 0)
        {
            diaLen = 4;
            foreach (string sentence in dialogue.sentences)
            {
                sentences.Add(sentence);
            }
            npcObj.dialogue_field = 4;


        }
        else if (npcObj.dialogue_field == 4)
        {
            for (int i = 0; i < Inventory.alchemySlots.Count; i++)
            {
                if (!Inventory.alchemySlots[i].GetComponent<Slot>().empty)
                {
                    if (Inventory.alchemySlots[i].GetComponent<Slot>().icon.name == Gifts[dialogue.name][0])
                    {
                        giftSlot = i;
                        npcObj.dialogue_field = 5;
                        SoundManagerScript.PlaySound("right");
                    }
                }
            }
            if (npcObj.dialogue_field == 4)
            {
                diaLen = 1;
                sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
                npcObj.affectionLevel = 0;
            }
            else 
            {
                diaLen = 2;
                sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
                sentences.Add(dialogue.sentences[npcObj.dialogue_field+1]);
                npcObj.affectionLevel = 2;
                globalInv.RemoveAlchemyItem(giftSlot);
                for (int i = 0; i < npcObj.affectionLevel; i += 1)
                {
                    hearts[i].SetActive(true);
                }
                npcObj.dialogue_field = 6;
            }
        }
        else if (npcObj.dialogue_field == 6)
        {
            for (int i = 0; i < Inventory.alchemySlots.Count; i++)
            {
                if (!Inventory.alchemySlots[i].GetComponent<Slot>().empty)
                {
                    if (Inventory.alchemySlots[i].GetComponent<Slot>().icon.name == Gifts[dialogue.name][1])
                    {
                        giftSlot = i;
                        npcObj.dialogue_field = 7;
                        SoundManagerScript.PlaySound("right");
                    }
                }
            }
            if (npcObj.dialogue_field == 6)
            {

                diaLen = 1;
                sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
                for (int i = 0; i < npcObj.affectionLevel; i += 1)
                {
                    hearts[i].SetActive(true);
                }
            }
            else
            {
                diaLen = 2;
                sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
                sentences.Add(dialogue.sentences[npcObj.dialogue_field+1]);
                npcObj.affectionLevel = 4;
                globalInv.RemoveAlchemyItem(giftSlot);
                for (int i = 0; i < npcObj.affectionLevel; i += 1)
                {
                    hearts[i].SetActive(true);
                }
                npcObj.dialogue_field = 8;
            }
        }
        else if (npcObj.dialogue_field == 8)
        {
            for (int i = 0; i < Inventory.alchemySlots.Count; i++)
            {
                if (!Inventory.alchemySlots[i].GetComponent<Slot>().empty)
                {
                    if (Inventory.alchemySlots[i].GetComponent<Slot>().icon.name == Gifts[dialogue.name][2])
                    {
                        giftSlot = i;
                        npcObj.dialogue_field = 9;
                        SoundManagerScript.PlaySound("right");
                    }
                }
            }
            if (npcObj.dialogue_field == 8)
            {

                diaLen = 1;
                sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
                for (int i = 0; i < npcObj.affectionLevel; i += 1)
                {
                    hearts[i].SetActive(true);
                }
            }
            else
            {
                diaLen = 2;
                sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
                sentences.Add(dialogue.sentences[npcObj.dialogue_field+1]);
                npcObj.affectionLevel = 6;
                globalInv.RemoveAlchemyItem(giftSlot);
                for (int i = 0; i < npcObj.affectionLevel; i += 1)
                {
                    hearts[i].SetActive(true);
                }
                callEndScene = true;
                npcObj.dialogue_field = 10;
            }
        }
        else if (npcObj.dialogue_field == 10)
        {
            diaLen = 1;
            sentences.Add(dialogue.sentences[npcObj.dialogue_field]);
            for (int i = 0; i < npcObj.affectionLevel; i += 1)
            {
                hearts[i].SetActive(true);
            }

        }

        DisplayNextSentence();
    }


    public void DisplayNextSentence()
    {
        SoundManagerScript.PlaySound("woodButton");
        //Debug.Log(sentences.Count);
        if (currCount == diaLen)
        {
            EndDialogue();
            return;
        }
        else if (currCount == diaLen - 1)
        {
            dialogueButton.GetComponentInChildren<Text>().text = "Exit";
        }
        else if (currCount == 1)
        {
            prevDialogueButton.gameObject.SetActive(true);
        }
        else if (currCount == 0)
        {
            prevDialogueButton.gameObject.SetActive(false);
        }

        string sentence = sentences[currCount];
        currCount += 1;
        //Debug.Log(sentence);
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    public void DisplayPrevSentence()
    {
        SoundManagerScript.PlaySound("woodButton");
        if (currCount == diaLen)
        {
            dialogueButton.GetComponentInChildren<Text>().text = "Next >>";
        }
        currCount -= 2;
        DisplayNextSentence();
    }
    IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    public void EndDialogue()
    {
        SoundManagerScript.PlaySound("dialogue");
        dialogueMode = false;
        dialogueStarted = false;
        animator.SetBool("IsOpen", false);
        //Debug.Log("End of Conversation.");
        if (callEndScene)
        {
            //Invoke("endcaller.checkEndScene", 3);
            endcaller.checkEndScene();
        }
    }
}
